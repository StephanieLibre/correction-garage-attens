<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ReadEmployeeVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return $attribute === "read_employee";
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if($user === $subject){
            return true;
        } else {
            return false;
        }
    }
}
